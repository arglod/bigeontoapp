var url_host = "http://crowd.fi.uncoma.edu.ar:3333/";

$(document).ready(function () {
    $('#send').click(function () {
        switch($('#convertid')[0].value){
            case "OWL Classes to Metamodel":
                url = url_host + "owlclassestometa";
                var settings = {
                        "url": url,
                        "method": "POST",
                        "timeout": 0,
                        "headers": {
                            "Content-Type": "text/plain",
                            "Access-Control-Allow-Origin":"*"
                        },
                        "data": $('#jsonInput')[0].value.toString(),
                    };
                break;
            case "All OWL SubClasses to Metamodel":
                url = url_host + "owlallsubstometa";
                var settings = {
                        "url": url,
                        "method": "POST",
                        "timeout": 0,
                        "headers": {
                            "Content-Type": "text/plain",
                            "Access-Control-Allow-Origin":"*"
                        },
                        "data": $('#jsonInput')[0].value.toString(),
                    };
                break;
            case "One OWL SubClass to Metamodel":
                url = url_host + "owlonesubstometa";
                var settings = {
                        "url": url,
                        "method": "POST",
                        "timeout": 0,
                        "headers": {
                            "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
                            "Access-Control-Allow-Origin":"*"
                        },
                        "data": {
                        	onto: $('#jsonInput')[0].value.toString(),
                        	entity: $('#jsonInput2')[0].value.toString()
                        }
                    };
                break;
            default:
                url = url_host + "owlclassestometa";
                break;
        };


        $.ajax(settings).done(function (response) {
            $(jsonOutput)[0].value = "";
            console.log(response);
            $(jsonOutput)[0].value = JSON.stringify(response, undefined, 4);
        });
    })
});

function beautify() {
    var ugly = document.getElementById('jsonOutput').value;
    var obj = JSON.parse(ugly);
    var pretty = JSON.stringify(obj, undefined, 4);
    document.getElementById('jsonOutput').value = pretty;
}

function copy(){
    $("#jsonOutput").select();
    document.execCommand('copy');
}

function cut(){
    $("#jsonOutput").select();
    document.execCommand('cut');
}



$('#convertid')
