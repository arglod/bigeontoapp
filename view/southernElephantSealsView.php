<?php

ini_set('display_errors', 'on');
require_once '../lib/sparqllib.php';
require_once '../model/southernElephantSealsModel.php';
require_once '../controller/southernElephantSealsController.php';

$model = new SouthernElephantSealsModel();
$controller = new SouthernElephantSealsController($model);
$view = new SouthernElephantSealsView($controller, $model);

class SouthernElephantSealsView {
  private $model;
  private $controller;
  private $geoPoints;

  public function __construct(SouthernElephantSealsController $controller, SouthernElephantSealsModel $model) {
      $this->controller = $controller;
      $this->model = $model;
      $this->geoPoints = [];
  }

  public function getGeoPoints(){
    return $this->geoPoints;
  }

  /**
  @return a point for map
  {"name": "point1",
   "lat": a_lat,
   "lon": a_lon}
  */
  public function getMapPoint($geoStr){
    $geo = explode(" ", explode("POINT",$geoStr, 2)[1], 2);
    $lon = explode("(", $geo[0], 2)[1];
    $lat = explode(")", $geo[1], 2)[0];

    $point = array('name' => 1, 'lat' => (float)$lat, 'lon' => (float)$lon);
    return $point;

  }

  public function showPlatforms() {
    $fields = sparql_field_array( $this->model->resultAllPlatforms );
    print "<p>Number of results: ".sparql_num_rows( $this->model->resultAllPlatforms )." .</p>";
    print "<table border='1'>";
    print "<tr>";

    foreach( $fields as $field ){
      print "<th>$field</th>";
    }
    print "</tr>";
    while( $row = sparql_fetch_array( $this->model->resultAllPlatforms ) ){
        $nombre="";
        print "<tr>";
        foreach( $fields as $field ){
            print "<td><a href='$row[$field]'>$row[$field]</a></td>";
        }
        print "</tr>";
    }
    print "</table>";
  }

  public function showGeoDataForAPlatform() {
    $fields = sparql_field_array( $this->model->resultGeoDataForPlat );
/*    print "<p>Number of results: ".sparql_num_rows( $this->model->resultGeoDataForPlat )." .</p>";
    print "<table border='1'>";
    print "<tr>";*/

/*    foreach( $fields as $field ){
      print "<th>$field</th>";
    }
    print "</tr>"; */
    while( $row = sparql_fetch_array( $this->model->resultGeoDataForPlat ) ){
        $nombre="";
//        print "<tr>";
        foreach( $fields as $field ){
        //    print "<td><a href='$row[$field]'>$row[$field]</a></td>";
            $point = $this->getMapPoint($row[$field]);
            array_push($this->geoPoints, $point);
        }
    //    print "</tr>";
    }
  //  print "</table>";
  }

}

 ?>
<!DOCTYPE html>
<html>
  <head>
        <title>Platforms</title>
        <link rel="stylesheet" type="text/css" href="../css/gilia.css"/>
        <script type="text/javascript" src="../js/jquery-1.12.1.min.js"></script>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
            integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
            integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
            crossorigin="anonymous"></script>

        <!-- Highcharts -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/proj4js/2.3.6/proj4.js"></script>
        <script src="https://code.highcharts.com/maps/highmaps.js"></script>
        <script src="https://code.highcharts.com/maps/modules/exporting.js"></script>
        <script src="https://code.highcharts.com/maps/modules/offline-exporting.js"></script>
        <script src="https://code.highcharts.com/mapdata/countries/gb/gb-all.js"></script>
        <script src="https://code.highcharts.com/mapdata/custom/world.js"></script>

        <script src="https://code.highcharts.com/mapdata/countries/ar/ar-all.js"></script>

        <script src="https://code.highcharts.com/mapdata/custom/south-america.js"></script>




    </head>

    <body>
      <div class="container">
          <div class="row">
              <div class="col-6">
                <div class="form-group">
                    <label for="points">GeoPoints</label>
                    <textarea class="form-control" id="points" rows="30"><?php
                      $controller->giveMeGeoDataForAPlatform();
                      $view->showGeoDataForAPlatform();
                      print_r(json_encode($view->getGeoPoints()));
                      ?></textarea>
                </div>
              </div>

              <div class="col-6">
                <div class="form-group">
                   <form>
                    <input type="button" class="btn btn-primary" id="plot" value="Plot" onclick="drawChart();"/>
                   </form>
                </div>
                <div id="container-arg"></div>
              </div>
          </div>
      </div>
    </body>

    <script src="map.js"></script>

</html>
<?php  ?>
