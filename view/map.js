var chart;


function getData(){
  var gData = JSON.parse($("#points")[0].value);
  return gData;
}

function drawChart() {

    data = getData();

    var options = {
      chart: {
        map: 'countries/ar/ar-all'
      },
      title: {
        text: 'Southern Elephant Seals'
      },
      mapNavigation: {
        enabled: true
      },
      tooltip: {
        headerFormat: '',
        pointFormat: '<b>{point.name}</b><br>Lat: {point.lat}, Lon: {point.lon}'
      },
      series: [{
        name: 'Basemap',
        borderColor: '#A0A0A0',
        nullColor: 'rgba(200, 200, 200, 0.3)',
        showInLegend: false
      }, {
        name: 'Separators',
        type: 'mapline',
        nullColor: '#707070',
        showInLegend: false,
        enableMouseTracking: false
      }, {
        type: 'mappoint',
        name: 'observations',
        color: Highcharts.getOptions().colors[1],
        data: data
      }]
    }
    chart = new Highcharts.mapChart('container-arg', options);
}
