<?php

require_once("../lib/sparqllib.php");

class SouthernElephantSealsController {

    public $model;

  	public function __construct(SouthernElephantSealsModel $model) {
    	$this->model = $model;
  	}

  	public function giveMeAllPlatforms() {
  		return $this->model->getAllPlatforms();
    }

    public function giveMeGeoDataForAPlatform() {
      $iri = "http://localhost:2020/resource/platform/SES_AAEU";
      return $this->model->getGeoDataPlatform($iri);
    }
}
