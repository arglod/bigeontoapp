<?php

require_once("../lib/sparqllib.php");

class SouthernElephantSealsModel {

       public $db_connection;
       public $resultAllPlatforms;
       public $resultGeoDataForPlat;

       /*

       SELECT DISTINCT ?s ?p WHERE {
         ?s rdf:type sosa:Platform .
       }
       LIMIT 100

       SELECT DISTINCT ?s ?pl ?loc WHERE {
        ?s rdf:type sosa:Platform .
        ?p vocab:observado_claveU ?s .
        ?p  rdf:type	dwc:Occurrence .
        ?p dwciri:inDescribedPlace ?pl .
        ?pl geo:asWKT ?loc
      }
      LIMIT 100
      */

       public function __construct() {
         //$ini_array = parse_ini_file("../linkeddata_config.ini", true);
         //$db = sparql_connect($ini_array["SPARQL_endpoints"]["url"]["local"]);
         $db = sparql_connect("http://localhost:2020/sparql");

         if( !$db ) {
           print sparql_errno() . ": " . sparql_error(). "\n"; exit;
         }
         else {
           sparql_ns("qudt","http://qudt.org/1.1/schema/qudt#");
           sparql_ns("owl", "http://www.w3.org/2002/07/owl#");
           sparql_ns("fabio","http://purl.org/spar/fabio/");
           sparql_ns("xsd","http://www.w3.org/2001/XMLSchema#");
           sparql_ns("datacite","http://purl.org/spar/datacite/");
           sparql_ns("rdfs","http://www.w3.org/2000/01/rdf-schema#");
           sparql_ns("geo","http://www.opengis.net/ont/geosparql#");
           sparql_ns("dwciri","http://rs.tdwg.org/dwc/iri/");
           sparql_ns("dwc","http://rs.tdwg.org/dwc/terms/");
           sparql_ns("rdf","http://www.w3.org/1999/02/22-rdf-syntax-ns#");
           sparql_ns("bigeonto","http://www.w3id.org/cenpat-gilia/bigeonto/");
           sparql_ns("vocab","http://localhost:2020/resource/vocab/");
           sparql_ns("time","http://www.w3.org/2006/time#");
           sparql_ns("map","http://localhost:2020/resource/#");
           sparql_ns("db","http://localhost:2020/resource/");
           sparql_ns("prism","http://prismstandard.org/namespaces/basic/2.0/");
           sparql_ns("foaf","http://xmlns.com/foaf/0.1/");
           sparql_ns("sosa","http://www.w3.org/ns/sosa/");
           sparql_ns("dc","http://purl.org/dc/terms/");

           $this->db_connection = $db;
         };
       }

       public function getAllPlatforms(){
         $sparql = "SELECT DISTINCT ?plat
                    WHERE {
                      ?plat rdf:type sosa:Platform .
                    } LIMIT 100";

         $result = sparql_query($sparql);

         if( !$result ) {
           print sparql_errno() . ": " . sparql_error(). "\n"; exit;
         }
         else {
           return $this->resultAllPlatforms = $result;
         }
       }

       /**
        Given a particular platform (Elephant), get the geo data associated to its observations

        @param iri as a String without brackets <>
        @return a table of geo:asWKT
       */
       /*
       SELECT ?pl ?loc WHERE {
               <http://localhost:2020/resource/platform/SES_AAEU> rdf:type sosa:Platform .
               ?p vocab:observado_claveU <http://localhost:2020/resource/platform/SES_AAEU> .
               ?p  rdf:type	dwc:Occurrence .
               ?p dwciri:inDescribedPlace ?pl .
               ?pl geo:asWKT ?loc
               }
      */
       public function getGeoDataPlatform($IRI){
         $platIRI = "<" . $IRI . ">";

         $sparql = "SELECT ?geo WHERE {" .
                      $platIRI . " rdf:type sosa:Platform .
                      ?ocurr vocab:observado_claveU " . $platIRI . " .
                      ?ocurr rdf:type	dwc:Occurrence .
                      ?ocurr dwciri:inDescribedPlace ?obs .
                      ?obs geo:asWKT ?geo
                    }";

         $result = sparql_query($sparql);

         if( !$result ) {
           print sparql_errno() . ": " . sparql_error(). "\n"; exit;
         }
         else {
           return $this->resultGeoDataForPlat = $result;
         }
       }

}
